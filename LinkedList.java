
//Q2:Reverse a Linked List

class RevList{
	
	static class Node{
		private int data;
		private Node next;
		
		public Node(){
			data = 0;
			next = null;
		}
		
		public Node(int val){
			data = val;
			next = null;
		}
	}
	
	private Node head;
	
	public RevList(){
		head = null;
	}
	
	public void addFirst(int val){
		Node newNode = new Node(val);
		newNode.next = head;
		head = newNode;
	}
	
	public void addLast(int val){
		Node newNode = new Node(val);
		if(head == null)
			addFirst(val);
		else{
			Node trav = head;
			while(trav.next != null)
				trav = trav.next;
			trav.next = newNode;
		}
		
	}
	
	public void display(){
		Node trav = head;
		while(trav != null){
			System.out.println(trav.data);
		trav = trav.next;
		}
	}
	
	public void revDisplay(Node h){
		if(h.next == null)
			return;
		revDisplay(h.next);
		System.out.println(h.data);
	}
	
	public void revDisplay(){
		Node h = head;
		revDisplay(h);
	}
	
}

class LinkedList{
	public static void main(String args[]){
		RevList l = new RevList();
		
		// to see result first comment from line no73 to 84 after that comment second from line no 84 to 93
		
		l.addFirst(5);
		l.addFirst(4);
		l.addFirst(3);
		l.addFirst(2);
		l.addFirst(1);
		System.out.println("Before test case1:\n");
		l.display();
		System.out.println("After test case1: \n");
		l.revDisplay();
		
		
		l.addFirst(5);
		l.addFirst(2);
		l.addFirst(4);
		l.addFirst(3);
		System.out.println("Before test case2:\n");
		l.display();
		System.out.println("After test case2:\n");
		l.revDisplay();
	
	}
}