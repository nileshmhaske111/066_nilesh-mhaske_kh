
// Que Sorting using insertion sort

import java.util.*;

class Sorting{
	
	public static void insertionSort(int arr[]){
		for(int i=1; i<arr.length;i++){
			int j, temp = arr[i];
			for(j=i-1;j>=0 && arr[j]>temp ;j--){
				arr[j+1] = arr[j];
				System.out.println("Before Insertion Sort: "+Arrays.toString(arr));
			}
			arr[j+1] = temp;
			System.out.println("==============================================");
		}
	}
	
	public static void main(String args[]){
		int arr[] = {1,2,4,5,3};
		insertionSort(arr);
		System.out.println("After InsertionSort:\n"+Arrays.toString(arr));
		
		int arr1[] = {2,4,6,8,3};
		insertionSort(arr1);
		System.out.println("After InsertionSort:\n"+Arrays.toString(arr1));
	}
}