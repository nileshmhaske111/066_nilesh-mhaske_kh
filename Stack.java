//Q3:Java program to implement two stacks in a single array.

import java.util.*;
class Stack{
	
	int arr[];
	int top1;
	int top2;
	
	Stack(int size){
		arr = new int[size];
		top1 =-1;
		top2 = arr.length;
	}
	
	public boolean isEmpty(){
		return (top1==-1 && top2==arr.length);	
	}
	
	public boolean isFull(){
		return (top1+1 == top2);
	}
	public void push1(int val){
		if(isFull())
			System.out.println("Stack is full");
		else{
			top1++;
			arr[top1] = val;
		}
	}
	
	
	public void pop1(){
		if(isEmpty())
			System.out.println("Stack is empty");
		else{
			top1--;
		
		}
	}
	
	
	public void push2(int val){
		if(isFull())
			System.out.println("Stack is full");
		else{
			top2--;
			arr[top2] = val;
		}
	}
	
	
	public void pop2(){
			if(isEmpty())
			System.out.println("Stack is empty");
		else{
			top2++;
		
		}
	}
	
	
	public void display(){
		for(int i=0;i<=top1;i++)
			System.out.println(arr[i]);
		for(int j=arr.length-1;j>=top2;j--)
			System.out.println(arr[j]);
	}
	
	public static void main(String args[]){

		Stack s = new Stack(6);
		
		
		s.push1(5);
		s.push2(10);
		s.push2(15);
		s.push1(11);
		s.push2(7);
		s.push2(40);
		System.out.println("Before pop");
		s.display();
		System.out.println("After pop");
		s.pop1();
		s.pop2();
		s.display();
		
	}
}